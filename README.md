# hearthstone-web

## A web clone of blizzard's card game 'hearthstone'
hearthstone-web is a JS web clone of blizzard's popular card game 'hearthstone' developed to run in browsers.

## Where can I play?
To play the hearthstone-web clone click [here](https://rymedy.github.io/hearthstone-web/).

Current preview of the game:
![This image failed to load.](https://github.com/Rymedy/hearthstone-web/blob/master/src/images/gamepreview.PNG)

## Where can I report glitches and bugs?
You can report glitches and bugs in the project by simply making a request [here](https://github.com/Rymedy/hearthstone-web/issues).

## Is this project still in development?
Yes, please note this project is still in major development and will contain bugs and undeveloped features.

These bugs and features will swiftly be patched and developed in future patches.

## Will you implement multiplayer functionality in the future?
If I have the time and motivation, of course!

## License
[MIT](https://choosealicense.com/licenses/mit/)

